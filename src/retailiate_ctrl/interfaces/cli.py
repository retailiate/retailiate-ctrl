# -*- coding: utf-8 -*-
import asyncclick as click
from pyckles import Pyckles

# fix for pyinstaller
from ruamel.yaml import YAML

from frtls.cli.logging import logzero_option_async

try:
    import dataclasses  # noqa
except (Exception):
    pass

from retailiate_ctrl.defaults import RETAILIATE_CTRL_FRECKLETS_FOLDER
from retailiate_ctrl.k8s import Kube
from retailiate_ctrl.k8s.app.install import install_retailiate

click.anyio_backend = "asyncio"

yaml = YAML()


@click.group()
@logzero_option_async()
@click.pass_context
def cli(ctx):

    context_config = ["callback=default::full", "keep_run_folder=true"]
    # context_config = []

    p = Pyckles(
        pkg_name="retailiate_frecklets",
        extra_repos=[RETAILIATE_CTRL_FRECKLETS_FOLDER],
        context_config=context_config,
    )

    ctx.obj = {}
    ctx.obj["pyckles"] = p


@cli.command()
@click.option(
    "--admin-password",
    prompt=True,
    hide_input=True,
    confirmation_prompt=False,
    help="password for the admin account",
)
@click.option(
    "--cloudflare-token",
    "-p",
    prompt=True,
    confirmation_prompt=False,
    required=True,
    hide_input=True,
)
@click.option("--context-name", "-c", help="the context name", default="default")
@click.argument("kubecfg_path", nargs=1, default="~/.kube/config")
@click.pass_context
async def install(
    ctx, kubecfg_path: str, context_name: str, admin_password, cloudflare_token: str
):

    kube = await Kube.create_obj(
        kubecfg_path=kubecfg_path, context_name=context_name, is_k3d=False
    )
    ctl = kube.ctl(
        "wait",
        "-n",
        "ikh",
        "--for=condition=ready",
        "pod",
        "-l",
        "app=retailiate-api",
        "--timeout=400s",
        raise_exception=False,
    )
    if ctl.success:
        click.echo("Retailiate already installed and ready.")
    else:
        click.echo(f"Starting retailiate install...'")
        await install_retailiate(
            kube=kube, admin_password=admin_password, cloudflare_token=cloudflare_token
        )


if __name__ == "__main__":
    cli()
