# -*- coding: utf-8 -*-
import os

import asyncclick as click
import sys
from pathlib import Path

# fix for pyinstaller
from ruamel.yaml import YAML

from frtls.cli.logging import logzero_option_async
from frtls.networking import find_free_port
from retailiate_ctrl.dev.dev_env import check_dev_env, install_dev_requirements
from retailiate_ctrl.k8s.utils import port_forward

try:
    import dataclasses  # noqa
except (Exception):
    pass

from retailiate_ctrl.k8s import Kube
from retailiate_ctrl.k8s.app.install import install_retailiate
from retailiate_ctrl.utils import execute

click.anyio_backend = "asyncio"

yaml = YAML()


@click.group()
@click.option(
    "--cluster-name",
    "-n",
    help="the name of the development cluster to connect to",
    default="retailiate",
)
@logzero_option_async()
@click.pass_context
async def cli(ctx, cluster_name):

    ctx.obj = {}
    ctx.obj["dev_cluster_name"] = cluster_name

    if ctx.invoked_subcommand != "cluster":
        kube = await Kube.create_obj(context_name=cluster_name, is_k3d=True)
    else:
        kube = None

    ctx.obj["kube"] = kube


@cli.group("env")
@click.pass_context
def env(ctx):
    pass


@env.command()
@click.pass_context
def check(ctx):

    check = check_dev_env()

    import pp

    pp(check)


@env.command()
@click.pass_context
def install(ctx):

    install_dev_requirements()


def build_folder(folder):

    build_file = os.path.join(folder, ".build.yaml")

    build_file = Path(build_file)
    if not build_file.exists():
        click.echo(
            f"No '.build.yaml' file in folder '{build_file.as_posix()}', ignoring..."
        )
        return

    yaml_content = yaml.load(build_file)

    if yaml_content is None:
        yaml_content = {}

    template = yaml_content.get("template")
    print(template)


@cli.command()
@click.argument("folders", nargs=-1, required=False)
@click.pass_context
async def build(ctx, folders):

    if not folders:
        folders = [os.getcwd()]

    for f in folders:
        build_folder(f)


@cli.group("cluster")
@click.pass_context
async def dev_cluster(ctx):

    if ctx.invoked_subcommand != "create":
        ctx.obj["kube"] = await Kube.create_obj(
            context_name=ctx.obj["dev_cluster_name"], is_k3d=True
        )


@dev_cluster.command("create")
@click.option(
    "--admin-password",
    prompt=False,
    hide_input=True,
    confirmation_prompt=False,
    help="password for the admin account (defaults to 'password123')",
    default="password123",
)
@click.option(
    "--cloudflare-token",
    prompt=False,
    confirmation_prompt=False,
    required=False,
    hide_input=True,
    default=None,
)
@click.option("--port", "-p", multiple=True, required=False)
@click.argument("cluster_name", nargs=1, required=False)
@click.pass_context
async def create_dev_cluster(ctx, admin_password, cloudflare_token, port, cluster_name):

    if cluster_name:
        if ctx.obj["dev_cluster_name"] and ctx.obj["dev_cluster_name"] != "retailiate":
            if ctx.obj["dev_cluster_name"] != cluster_name:
                click.echo(
                    f"Can't create development cluster: both '--cluster-name/-n <cluster_name' option ({ctx.obj['dev_cluster_name']}) and cluster name ({cluster_name}) argument specified, ."
                )
                sys.exit(1)

    else:
        cluster_name = ctx.obj["dev_cluster_name"]

    rc, stdout, stderr = execute("k3d", "list")
    if rc == 1 or cluster_name not in stdout:

        api_port = find_free_port(6443)

        click.echo(
            f"Creating k3d cluster '{cluster_name}' (using api port '{api_port}')..."
        )

        args = [
            "create",
            "--api-port",
            str(api_port),
            "--server-arg",
            "--no-deploy=traefik",
            "--name",
            cluster_name,
            "--server-arg",
            "--kube-apiserver-arg=runtime-config=settings.k8s.io/v1alpha1=true",
            "--server-arg",
            "--kube-apiserver-arg=enable-admission-plugins=DefaultStorageClass,DefaultTolerationSeconds,LimitRanger,NamespaceLifecycle,NodeRestriction,PersistentVolumeLabel,ResourceQuota,ServiceAccount,PodPreset",
        ]

        pfs = []
        if port:
            for pf in port:
                if ":" in pf:
                    pf_string = pf
                else:
                    pf_string = f"{pf}:{pf}"
                pfs.append("--publish")
                pfs.append(pf_string)

        args.extend(pfs)

        rc, stdout, stderr = execute("k3d", args)

        if rc != 0:
            click.echo("Cluster creation failed: {}".format(stderr))
            sys.exit(1)

    kube = await Kube.create_obj(is_k3d=True, context_name=cluster_name)
    ctl = await kube.ctl(
        "wait",
        "-n",
        "ikh",
        "--for=condition=ready",
        "pod",
        "-l",
        "app=retailiate-api",
        "--timeout=400s",
        raise_exception=False,
    )
    if await ctl.success:
        click.echo("Retailiate already installed and ready.")
    else:
        click.echo(f"Installing retailiate into dev cluster '{cluster_name}'")
        await install_retailiate(
            kube=kube, admin_password=admin_password, cloudflare_token=cloudflare_token
        )


@cli.command("port-forward")
@click.option(
    "--port",
    "-p",
    help="the port to forward to (will be used as starting point if already taken)",
    default=None,
)
@click.option(
    "--open-browser-tab", "-o", help="open browser tab with the right url", is_flag=True
)
@click.argument("service_name", nargs=-1, required=True)
@click.pass_context
async def pf(ctx, port, open_browser_tab, service_name):

    kube = ctx.obj["kube"]

    click.echo()
    await port_forward(
        kube=kube, services=service_name, port=port, open_browser_tab=open_browser_tab
    )

    sys.exit()


if __name__ == "__main__":
    cli()
