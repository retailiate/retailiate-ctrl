# -*- coding: utf-8 -*-
import os

import sys
from appdirs import AppDirs

retailiate_ctrl_app_dirs = AppDirs("retailiate_ctrl", "frkl")

if not hasattr(sys, "frozen"):
    RETAILIATE_CTRL_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `retailiate_ctrl` module."""
else:
    RETAILIATE_CTRL_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "retailiate_ctrl")
    """Marker to indicate the base folder for the `retailiate_ctrl` module."""

RETAILIATE_CTRL_RESOURCES_FOLDER = os.path.join(
    RETAILIATE_CTRL_MODULE_BASE_FOLDER, "resources"
)
RETAILIATE_K8S_RESOURCES_FOLDER = os.path.join(
    RETAILIATE_CTRL_RESOURCES_FOLDER, "retailiate_resources"
)
RETAILIATE_CTRL_FRECKLETS_FOLDER = os.path.join(
    RETAILIATE_CTRL_RESOURCES_FOLDER, "frecklets"
)

RETAILIATE_CTRL_SHARE_FOLDER = os.path.join(
    retailiate_ctrl_app_dirs.user_data_dir, "share"
)
RETAILIATE_CTRL_BIN_FOLDER = os.path.join(retailiate_ctrl_app_dirs.user_data_dir, "bin")

_path = os.environ.get("PATH", os.defpath)
_path = _path.split(os.pathsep)

RETAILIATE_PATH = [RETAILIATE_CTRL_BIN_FOLDER] + _path
