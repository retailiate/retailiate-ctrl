# -*- coding: utf-8 -*-
import atexit
import os
import re
import tempfile

import asyncio
import kubernetes_asyncio
from asyncio.subprocess import Process
from kubernetes_asyncio.client import ApiClient, CoreV1Api, AppsV1Api
from kubernetes_asyncio.config.kube_config import KUBE_CONFIG_DEFAULT_LOCATION
from ruamel.yaml import YAML
from time import sleep
from typing import Dict, Union

from retailiate_ctrl.utils import execute

yaml = YAML()
yaml.default_flow_style = False


class KubeCtlException(Exception):
    def __init__(self, command, args, retcode, stdout, stderr):

        msg = f"Error executing kubectl command '{command}': {retcode} - {stdout}/{stderr}"
        super(KubeCtlException, self).__init__(msg)

        self._msg = msg
        self._command = command
        self._args = args
        self._retcode = retcode
        self._stdout = stdout
        self._stderr = stderr


class KubeCtl(object):
    def __init__(self, kube: "Kube", command: str, *args, expected_ret_code=0):

        self._kube = kube
        self._command = command
        self._args = args
        self._cmd = [self._command] + list(self._args)
        self._proc: Process = None
        self._expected_ret_code = expected_ret_code
        self._raise_exception = True

        self._return_code = None
        self._stdout = None
        self._stderr = None
        self._success = None

    async def run(self, wait=True, expected=None, raise_exception=True) -> Process:

        self._raise_exception = raise_exception
        if expected is not None:
            if not isinstance(expected, int):
                raise TypeError("'expected' value needs to be integer.")
            self._expected_ret_code = expected

        env = dict(os.environ)  # make a copy of the environment
        lp_key = "LD_LIBRARY_PATH"  # for Linux and *BSD.
        lp_orig = env.get(lp_key + "_ORIG")  # pyinstaller >= 20160820 has this
        xdb_key = "XDG_DATA_DIRS"
        xdb_orig = env.get(xdb_key + "_ORIG")

        if lp_orig:
            env[lp_key] = lp_orig
        else:
            lp = env.get(lp_key, None)
            if lp is not None and re.search(r"/_MEI......", lp):
                env.pop(lp_key)
        if xdb_orig:
            env[xdb_key] = xdb_orig

        self._proc = await asyncio.create_subprocess_exec(
            "kubectl",
            *self._cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
            env=env,
        )

        if wait:
            await self.wait()

        return self._proc

    @property
    async def returncode(self):

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._return_code

    @property
    async def stdout(self):

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._stdout

    @property
    async def stderr(self):

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._stderr

    @property
    async def success(self):

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._success

    async def wait(self) -> bool:

        if self._proc is None:
            raise Exception("No running process to wait on.")

        if self._success is not None:
            return self._success

        (stdout, stderr) = await self._proc.communicate()

        self._stdout = stdout.decode().split("\n")
        self._stderr = stderr.decode().split("\n")

        self._return_code = self._proc.returncode

        self._success = self._return_code == self._expected_ret_code

        if not self._success and self._raise_exception:
            raise KubeCtlException(
                command=self._command,
                args=self._args,
                retcode=self._return_code,
                stdout=self._stdout,
                stderr=self._stderr,
            )

        return self._success

    async def __aenter__(self):

        await self.run(wait=False, raise_exception=True)
        # self.run(wait=False)
        return self

    async def kill(self, wait=True):

        if self._proc is None:
            raise Exception("No process started yet.")

        try:
            self._proc.kill()
        except (ProcessLookupError):
            pass
        if wait:
            pass
            # await self._proc.wait()

    async def __aexit__(self, *args):

        try:
            await self.kill()
        except (Exception) as e:
            import traceback

            traceback.print_exc()
            print(e)

    def __repr__(self):

        started = self._proc is not None
        finished = self._success is not None
        if finished:
            status = "finished"
            msg = f", ret_code={self._return_code}"
        else:
            status = "started" if started else "no process"
            msg = ""
        return f"[KubeCtl: command={self._command}, status={status}{msg}]"


class Kube(object):
    @classmethod
    async def create_obj(
        cls, kubecfg_path: str = None, context_name: str = None, is_k3d=False
    ):
        self = Kube(kubecfg_path=kubecfg_path, context_name=context_name, is_k3d=is_k3d)
        await self._load()
        return self

    def __init__(
        self, kubecfg_path: str = None, context_name: str = None, is_k3d=False
    ):

        if is_k3d and kubecfg_path is None:
            if context_name is None:
                context_name = "retailiate-dev"

            k_path = os.path.expanduser(f"~/.config/k3d/{context_name}/kubeconfig.yaml")
            if not os.path.exists(k_path):
                i = 0
                k_path = None
                while i < 10:
                    rc, stdout, stderr = execute(
                        "k3d", "get-kubeconfig", "--name", context_name
                    )
                    if rc != 0:
                        sleep(1)
                        continue
                    else:
                        k_path = stdout.strip()
                        break

                if k_path is None:
                    raise Exception(
                        f"Can't get kubeconfig for k3d cluster '{context_name}'."
                    )

            kubecfg_path = k_path

        else:
            if kubecfg_path is None:
                kubecfg_path = KUBE_CONFIG_DEFAULT_LOCATION

            kubecfg_path = os.path.expanduser(kubecfg_path)

        self._kubecfg_path = kubecfg_path
        self._context_name = context_name

        self._kubecfg_private = None
        self._kube_config = None
        self._api_client = None
        self._core_api = None
        self._apps_api = None

        atexit.register(self._finalize)

    def _finalize(self):

        if self._kubecfg_private and os.path.exists(self._kubecfg_private):
            os.unlink(self._kubecfg_private)

    async def _load(self):

        self._kube_config = kubernetes_asyncio.client.Configuration()

        with open(self._kubecfg_path, "r") as f:
            kubeconfig = yaml.load(f)

        if self._context_name is not None:
            kubeconfig["current-context"] = self._context_name
        else:
            self._context_name = kubeconfig.get("current-context")
            if not self._context_name:
                self._context_name = "default"
                kubeconfig["current-context"] = self._context_name

        temp = tempfile.NamedTemporaryFile(mode="w", delete=False)
        self._kubecfg_private = temp.name
        yaml.dump(kubeconfig, temp)
        temp.close()

        loader = kubernetes_asyncio.config.kube_config.KubeConfigLoader(kubeconfig)
        await loader.load_and_set(self._kube_config)
        os.environ["KUBECONFIG"] = self._kubecfg_private

    @property
    def api_client(self) -> ApiClient:

        if self._api_client is not None:
            return self._api_client

        self._api_client = ApiClient(self._kube_config)
        return self._api_client

    @property
    def apps_api(self) -> AppsV1Api:

        if self._apps_api is not None:
            return self._apps_api

        self._apps_api = AppsV1Api(self._api_client)
        return self._apps_api

    @property
    def core_api(self) -> CoreV1Api:

        if self._core_api is not None:
            return self._core_api

        self._core_api = CoreV1Api(self.api_client)
        return self._core_api

    async def ctl(
        self, command: str, *args, wait=True, raise_exception=True, no_run=False
    ) -> KubeCtl:

        ctl = KubeCtl(self, command, *args)

        if not no_run:
            await ctl.run(wait=wait, raise_exception=raise_exception)
        return ctl

    def ctl_context(self, command: str, *args, **kwargs) -> KubeCtl:

        ctl = KubeCtl(self, command, *args, **kwargs)
        return ctl

    def port_forward(self, *args, **kwargs) -> KubeCtl:

        return self.ctl_context("port-forward", *args, **kwargs)

    async def apply(self, data: Union[Dict, str], wait=True, raise_exception=True):

        if isinstance(data, str):
            ctl = self.ctl(
                "apply", "-f", data, wait=wait, raise_exception=raise_exception
            )
            return ctl

        else:

            with tempfile.NamedTemporaryFile(mode="w") as temp:

                yaml.dump(data, temp)
                temp.flush()
                ctl = await self.ctl(
                    "apply", "-f", temp.name, wait=wait, raise_exception=raise_exception
                )
                return ctl

    async def create(self, data: Union[Dict, str], wait=True, raise_exception=True):

        if isinstance(data, str):
            ctl = await self.ctl(
                "create", "-f", data, wait=wait, raise_exception=raise_exception
            )
            return ctl

        else:
            with tempfile.NamedTemporaryFile(mode="w") as temp:

                yaml.dump(data, temp)
                temp.flush()
                ctl = await self.ctl(
                    "create",
                    "-f",
                    temp.name,
                    wait=wait,
                    raise_exception=raise_exception,
                )
                return ctl

    async def delete(self, data: Union[Dict, str], wait=True, raise_exception=True):

        if isinstance(data, str):
            ctl = await self.ctl(
                "delete", "-f", data, wait=wait, raise_exception=raise_exception
            )
            return ctl

        else:
            with tempfile.NamedTemporaryFile(mode="w") as temp:

                yaml.dump(data, temp)
                temp.flush()
                ctl = await self.ctl(
                    "delete",
                    "-f",
                    temp.name,
                    wait=wait,
                    raise_exception=raise_exception,
                )
                return ctl

    async def wait(
        self, condition: str, labels: Dict = None, timeout=300, raise_exception=True
    ):

        cmd_list = [
            "wait",
            "-n",
            "ikh-mgmt",
            f"--for={condition}",
            "pod",
            f"--timeout={timeout}s",
        ]

        if labels:

            labels_list = []
            for k, v in labels.items():
                labels_list.append("-l")
                labels_list.append(f"{k}={v}")

            cmd_list.extend(labels_list)

        ctl = await self.ctl(*cmd_list, raise_exception=raise_exception)

        return ctl
