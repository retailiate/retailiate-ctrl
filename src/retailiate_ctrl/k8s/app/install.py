# -*- coding: utf-8 -*-

import json
import os

import aiohttp
import asyncio
import click
from kubernetes_asyncio.client import V1Secret
from pathlib import Path
from ruamel.yaml import YAML
from typing import Union, List

from retailiate_ctrl.defaults import RETAILIATE_K8S_RESOURCES_FOLDER
from retailiate_ctrl.k8s import Kube, KubeCtlException

yaml = YAML()


async def install_retailiate(
    kube: Kube,
    admin_password: str,
    domains: Union[str, List] = None,
    cloudflare_token=None,
):

    if not domains:
        domains = ["dev.retailiate.io"]
    elif isinstance(domains, str):
        domains = [domains]

    main_domain = domains[0]

    augmented_domains = []
    for d in domains:
        augmented_domains.append(d)
        augmented_domains.append(f"*.{d}")

    click.echo("Installing argocd...")
    crd_yaml = os.path.join(RETAILIATE_K8S_RESOURCES_FOLDER, "00-crds.yaml")
    click.echo("  - creating base crds")
    await kube.ctl("apply", "-f", crd_yaml)

    ns_yaml = os.path.join(RETAILIATE_K8S_RESOURCES_FOLDER, "01-namespaces.yaml")
    click.echo("  - creating required base namespaces")
    await kube.ctl("apply", "-f", ns_yaml)

    if cloudflare_token is not None:
        body = V1Secret(
            string_data={"api-key": cloudflare_token},
            kind="Secret",
            api_version="v1",
            metadata={"name": "cloudflare-api-key", "namespace": "cert-manager"},
        )
        await kube.core_api.create_namespaced_secret(
            namespace="cert-manager", body=body
        )

        ns_yaml = os.path.join(
            RETAILIATE_K8S_RESOURCES_FOLDER, "02-project-cert-issuer-letsencrypt.yaml"
        )
    else:
        ns_yaml = os.path.join(
            RETAILIATE_K8S_RESOURCES_FOLDER, "02-project-cert-issuer-self-signed.yaml"
        )

    click.echo("  - creating cert issuer resource")
    await kube.ctl("apply", "-f", ns_yaml)

    wc_yaml = Path(
        os.path.join(RETAILIATE_K8S_RESOURCES_FOLDER, "03-wildcard-cert.yaml")
    )
    wc_yaml_content = yaml.load(wc_yaml)

    wc_yaml_content["spec"]["commonName"] = main_domain
    wc_yaml_content["spec"]["dnsNames"] = augmented_domains

    click.echo("  - creating wildcard cert resource")
    await kube.apply(wc_yaml_content)

    click.echo("  - installing argocd")
    install_yaml = os.path.join(RETAILIATE_K8S_RESOURCES_FOLDER, "04-install.yaml")
    await kube.ctl("apply", "-n", "ikh-mgmt", "-f", install_yaml)

    click.echo("  - waiting for argocd...")
    while True:
        ctl = await kube.ctl(
            "wait",
            "-n",
            "ikh-mgmt",
            "--for=condition=ready",
            "pod",
            "-l",
            "app.kubernetes.io/name=argocd-server",
            "--timeout=300s",
            raise_exception=False,
        )
        if await ctl.returncode == 0:
            break
        else:
            await asyncio.sleep(2)

    click.echo("  - argocd ready")

    ret = await kube.core_api.list_namespaced_pod(namespace="ikh-mgmt")
    pw = None
    for i in ret.items:
        if "argocd-server" in i.metadata.name:
            pw = i.metadata.name
            break

    if not pw:
        raise Exception("No argocd-server pod")

    command = "port-forward"
    args = ["svc/argocd-server", "-n", "ikh-mgmt", "8081:443"]

    async with kube.ctl_context(command, *args):

        async with aiohttp.ClientSession() as session:

            # to make sure port-forwarding is ready
            await asyncio.sleep(2)

            try:
                data = {"username": "admin", "password": pw}

                click.echo("  - changing argo password")
                await session.post(
                    "https://localhost:8081/api/v1/session",
                    data=json.dumps(data).encode(),
                    ssl=False,
                )
                # result = await result.json()
                # token = result["token"]

                data = {"currentPassword": pw, "newPassword": admin_password}
                await session.put(
                    "https://localhost:8081/api/v1/account/password",
                    data=json.dumps(data).encode(),
                    ssl=False,
                )
                click.echo("  - argo default password changed")
            except (Exception):
                click.echo("  - password not changed, assuming it is already changed")

    click.echo("Installing retailiate")
    app_yaml = os.path.join(RETAILIATE_K8S_RESOURCES_FOLDER, "05-retailiate.yaml")
    await kube.ctl("apply", "-n", "ikh-mgmt", "-f", app_yaml)

    click.echo("  - waiting for retailiate install to finish")
    while True:
        try:
            await kube.ctl(
                "wait",
                "-n",
                "ikh",
                "--for=condition=ready",
                "pod",
                "-l",
                "app=retailiate-api",
                "--timeout=400s",
            )
            break
        except (KubeCtlException):
            await asyncio.sleep(2)
    click.echo("  - retailiate ready")
