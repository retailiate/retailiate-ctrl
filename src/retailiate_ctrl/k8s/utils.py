# -*- coding: utf-8 -*-
import json

import asyncclick as click
import asyncio
from kubernetes_asyncio import config, client
from typing import List, Union

from frtls.networking import find_free_port
from frtls.processes import open_url_in_browser
from retailiate_ctrl.k8s import Kube, KubeCtl

APP_MAP = {
    "argocd": {
        "target_port": 2000,
        "service_name": "svc/argocd-server",
        "namespace": "ikh-mgmt",
        "source_port": 443,
        "is_ssl": True,
    },
    "hasura": {
        "target_port": 2010,
        "service_name": "svc/hasura",
        "namespace": "ikh",
        "source_port": 8080,
        "is_ssl": False,
    },
    "openfaas": {
        "target_port": 2020,
        "service_name": "svc/gateway-external",
        "namespace": "ikh-mgmt-openfaas",
        "source_port": 8080,
        "is_ssl": False,
    },
    "minio": {
        "target_port": 2030,
        "service_name": "svc/minio-hl-svc",
        "namespace": "ikh",
        "source_port": 9000,
        "is_ssl": False,
    },
}


async def port_forward(
    kube: Kube,
    services: Union[List[str], str],
    port: int = None,
    open_browser_tab: bool = False,
):

    if isinstance(services, str):
        services = [services]

    if "all" in services:
        services = APP_MAP.keys()

    tasks = []

    for s in services:

        task = await port_forward_service(
            kube=kube, service=s, port=port, open_browser_tab=open_browser_tab
        )
        tasks.append(task)

    wait_for_user_input()

    for t in tasks:
        await t.kill(wait=True)

    # for t in tasks:
    #     print(t)
    #     print(t._cmd)
    #     print(await t.stdout)
    #     print(await t.stderr)


async def port_forward_service(
    kube: Kube, service: str, port: int = None, open_browser_tab: bool = False
) -> KubeCtl:

    if service not in APP_MAP.keys():
        raise ValueError(
            f"Service '{service}' not available, vailid services: {', '.join(APP_MAP.keys)}"
        )

    data = APP_MAP[service]

    if ":" in service:
        service, port = service.split(":")
        port = int(port)

    if port is None:
        port = data["target_port"]

    port = find_free_port(port)
    protocol = "https" if data["is_ssl"] else "http"
    url = f"{protocol}://localhost:{port}"
    args = [
        data["service_name"],
        "-n",
        data["namespace"],
        f"{port}:{data['source_port']}",
    ]

    kubectl = kube.port_forward(*args, expected_ret_code=-9)
    await kubectl.run(wait=False)

    click.echo(f"Forwarding {service} ui to: {url}")

    if open_browser_tab:
        await asyncio.sleep(0.35)

        await open_url_in_browser(url)

    return kubectl


def wait_for_user_input():

    c = None
    click.echo()
    click.echo("Press 'q' to stop the forwarding process...")
    while c != "q":
        c = click.getchar()

    click.echo()
    click.echo("Stopped...")


async def manual_post_to_service():

    await config.load_kube_config()

    v1 = client.CoreV1Api()
    # print("Listing pods with their IPs:")
    ret = await v1.list_namespaced_pod(namespace="ikh-mgmt")

    pw = None
    for i in ret.items:
        if "argocd-server" in i.metadata.name:
            pw = i.metadata.name
            break

    if not pw:
        raise Exception("No argocd-server pod")

    path = "api/v1/session"

    body = {"username": "admin", "password": pw}

    argo_cd_post_data = {
        "resource_path": "/api/v1/namespaces/{namespace}/services/{name}/proxy/{path}",
        "response_type": "str",
        "auth_settings": ["BearerToken"],
        "async_req": False,
        "_return_http_data_only": True,
    }

    get_bearer_token_data = {
        "method": "POST",
        "path_params": {
            "name": "https:argocd-server:443",
            "namespace": "ikh-mgmt",
            "path": path,
        },
        "body": body,
        "header_params": {"Accept": "*/*"},
    }

    get_bearer_token_data.update(argo_cd_post_data)

    response = v1.api_client.call_api(**get_bearer_token_data)

    resp = await response
    resp = resp.replace("'", '"')
    token = json.loads(resp)["token"]

    print("TOKEN: {}".format(token))

    path = "api/v1/account/password"
    set_password_data = {
        "method": "PUT",
        "body": {"currentPassword": pw, "newPassword": "nixenixe25"},
        "path_params": {
            "name": "https:argocd-server:443",
            "namespace": "ikh-mgmt",
            "path": path,
        },
        "header_params": {"Accept": "*/*", "Authorization": f"Bearer {token}"},
    }

    set_password_data.update(argo_cd_post_data)

    response = v1.api_client.call_api(**set_password_data)
