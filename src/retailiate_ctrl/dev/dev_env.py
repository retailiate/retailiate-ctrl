# -*- coding: utf-8 -*-
import getpass
import grp

import asyncclick as click
from pyckles import Pyckles

# fix for pyinstaller
from frtls.processes import command_exists

try:
    import dataclasses  # noqa
except (Exception):
    pass

from retailiate_ctrl.defaults import RETAILIATE_CTRL_BIN_FOLDER, RETAILIATE_PATH
from retailiate_ctrl.defaults import RETAILIATE_CTRL_FRECKLETS_FOLDER


def check_dev_env():

    check = {}

    check["docker"] = command_exists("docker", extra_path=RETAILIATE_PATH)

    username = getpass.getuser()
    docker_group = False
    for gr in grp.getgrall():
        gr_name = gr.gr_name
        if gr_name != "docker":
            continue
        mem = gr.gr_mem
        docker_group = username in mem
        break

    check["docker_group"] = docker_group

    check["k3d"] = command_exists("k3d", extra_path=RETAILIATE_PATH)
    check["kubectl"] = command_exists("kubectl", extra_path=RETAILIATE_PATH)
    check["helm"] = command_exists("helm", extra_path=RETAILIATE_PATH)
    check["argo"] = command_exists("argo", extra_path=RETAILIATE_PATH)

    return check


def install_dev_requirements():

    always_install = False

    check = check_dev_env()

    all_available = True

    for v in check.values():
        if not v:
            all_available = False
            break

    if all_available:
        click.echo("All dependencies available, doing nothing...")
        return

    installing = []

    context_config = ["callback=default::full", "keep_run_folder=true"]
    # context_config = []

    pyckles = Pyckles(
        pkg_name="retailiate_frecklets",
        extra_repos=[RETAILIATE_CTRL_FRECKLETS_FOLDER],
        context_config=context_config,
        init_module=False,
    )

    pycklets = []

    docker_exists = check["docker"]
    docker_group = check["docker_group"]
    username = getpass.getuser()

    if not docker_group or not docker_exists or always_install:

        p = pyckles.create_pycklet("docker-service-fix")
        p.users = [username]
        pycklets.append(p)
        installing.append("docker (needs sudo permission)")

    k3d_exists = command_exists("k3d", extra_path=RETAILIATE_PATH)

    if not k3d_exists or always_install:

        p = pyckles.create_pycklet("executable-downloaded")
        p.url = (
            "https://github.com/rancher/k3d/releases/download/v1.4.0/k3d-linux-amd64"
        )
        p.exe_name = "k3d"
        p.extract = False
        p.owner = username
        p.dest = RETAILIATE_CTRL_BIN_FOLDER

        pycklets.append(p)
        installing.append("k3d")

    kubectl_exists = command_exists("kubectl", extra_path=RETAILIATE_PATH)

    if not kubectl_exists or always_install:

        p = pyckles.create_pycklet("executable-downloaded")
        p.url = "https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl"
        p.exe_name = "kubectl"
        p.extract = False
        p.owner = username
        p.dest = RETAILIATE_CTRL_BIN_FOLDER

        pycklets.append(p)
        installing.append("kubectl")

    if pycklets:
        click.echo("Installing:")
        for i in installing:
            click.echo(f"  - {i}")

        pyckles.run_pycklets(pycklets)
