# -*- coding: utf-8 -*-
import os

from plumbum import local

from retailiate_ctrl.defaults import RETAILIATE_PATH


def execute(command, *args) -> str:

    env = dict(os.environ)  # make a copy of the environment
    lp_key = "LD_LIBRARY_PATH"  # for Linux and *BSD.
    lp_orig = env.get(lp_key + "_ORIG")  # pyinstaller >= 20160820 has this

    with local.env(PATH=os.pathsep.join(RETAILIATE_PATH)):

        if lp_orig is not None:
            local.env[lp_key] = lp_orig  # restore the original, unmodified value
        else:
            local.env.pop(lp_key, None)  # last resort: remove the env var

        cmd = local[command]
        rc, stdout, stderr = cmd.run(args, retcode=None)

        return (rc, stdout, stderr)
