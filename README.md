[![PyPI status](https://img.shields.io/pypi/status/retailiate-ctrl.svg)](https://pypi.python.org/pypi/retailiate-ctrl/)
[![PyPI version](https://img.shields.io/pypi/v/retailiate-ctrl.svg)](https://pypi.python.org/pypi/retailiate-ctrl/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/retailiate-ctrl.svg)](https://pypi.python.org/pypi/retailiate-ctrl/)
[![Pipeline status](https://gitlab.com/frkl/retailiate-ctrl/badges/develop/pipeline.svg)](https://gitlab.com/frkl/retailiate-ctrl/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# retailiate-ctrl

*Control module for retailiate infrastructure.*


## Download

```
wget  https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/retailiate-ctrl
chmod +x retailiate-ctrl
```

## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'retailiate-ctrl' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 retailiate_ctrl
    git clone https://gitlab.com/frkl/retailiate_ctrl
    cd <retailiate_ctrl_dir>
    pyenv local retailiate_ctrl
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
